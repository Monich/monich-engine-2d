message(STATUS "Configuring MonichEngine2D")

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_LIST_DIR}/Modules/")

include(${CMAKE_CURRENT_LIST_DIR}/Link/devil.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Link/glew.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Link/glfw3.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Link/glm.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Link/me2d.cmake)

macro(linkME2D project)
    message(STATUS "Linking ME2D to project")
    target_link_libraries(${project} ${ME2D_LIBRARY})
    target_link_libraries(${project} ${DEVIL_LIBRARY_1} ${DEVIL_LIBRARY_2} ${DEVIL_LIBRARY_3})
    target_link_libraries(${project} ${GLEW_LIBRARY})
    target_link_libraries(${project} ${GLFW3_LIBRARY})
    target_link_libraries(${project} GL)
endmacro()

message(STATUS "MonichEngine2D configured. Remember to link it to your project using linkME2D(project)" )

