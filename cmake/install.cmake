file(GLOB_RECURSE LINK_FILES "cmake/Link/*.cmake")
file(GLOB_RECURSE FIND_FILES "cmake/Modules/*.cmake")

install(TARGETS ME2D ARCHIVE DESTINATION lib)

install(FILES ${ME2D_INCS} DESTINATION include/monich2d)
install(FILES ${ME2H_INCS} DESTINATION include/monich2dhelpers)
install(FILES ${ME2SH_INCS} DESTINATION include/monich2dshaders)
install(FILES cmake/ME2DConfig.cmake DESTINATION lib/cmake/ME2D)
install(FILES ${LINK_FILES} DESTINATION lib/cmake/ME2D/Link)
install(FILES ${FIND_FILES} DESTINATION lib/cmake/ME2D/Modules)
