#include "SimpleObject.h"

SimpleObject::SimpleObject() : position(0), rotation(0), scale(1), mesh(NULL), mat(0)
{}

SimpleObject::SimpleObject(Mesh * m, uint32 materialType): position(0), rotation(0), scale(1), mesh(m), mat(materialType)
{}

SimpleObject::~SimpleObject()
{}

Mesh * SimpleObject::GetMesh() const
{
	return mesh;
}

uint32 SimpleObject::GetMaterial()
{
	return mat;
}

glm::mat4 *TODELETE SimpleObject::GetModelMatrix()
{
	return PrepareModelMatrix(position, rotation, scale);
}

void SimpleObject::SetPosition(glm::vec2 newPos)
{
	position = newPos;
}

void SimpleObject::SetPosition(float x, float y)
{
	position = glm::vec2(x, y);
}

void SimpleObject::SetRotation(glm::vec2 newRot)
{
	rotation = newRot;
}

void SimpleObject::SetRotation(float xRot, float yRot)
{
	rotation = glm::vec2(xRot, yRot);
}

void SimpleObject::SetScale(glm::vec2 newScale)
{
	scale = newScale;
}

void SimpleObject::SetScale(float newScale)
{
	scale = glm::vec2(newScale);
}

void SimpleObject::SetScale(float xScale, float yScale)
{
	scale = glm::vec2(xScale, yScale);
}

void SimpleObject::SetMesh(Mesh * newMesh)
{
	mesh = newMesh;
}

void SimpleObject::SetMaterial(uint32 newMaterial)
{
	mat = newMaterial;
}

glm::vec2 SimpleObject::GetPosition() const
{
	return position;
}

glm::vec2 SimpleObject::GetRotation() const
{
	return rotation;
}

glm::vec2 SimpleObject::GetScale() const
{
	return scale;
}
