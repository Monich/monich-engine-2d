#ifndef M2D_MOUSEEVENT_H
#define M2D_MOUSEEVENT_H

#include "defines.h"

enum MouseEventButton
{
	MOUSEBUTTON_LEFT,
	MOUSEBUTTON_RIGHT,
	MOUSEBUTTON_ELSE,
};

struct MouseEvent
{
	uint32 button;
	float64 x, y;

	MouseEvent(uint32 b, float64 x, float64 y);
};

#endif //!M2D_MOUSEEVENT_H
