#include "Settings.h"

#include <string>

#define CONFIG_FILE_NAME "config.ini"

Settings::Settings() : fields(NULL), cstrings(NULL), saved(false), loaded(false)
{
    fields = new BigField[_getFieldSize()];
    cstrings = new cstring[_getCStringFieldSize()];
}

Settings::~Settings()
{
    if (fields)
        delete fields;

    if (cstrings)
        delete[] cstrings;
}

bool Settings::Load()
{
    std::ifstream file(CONFIG_FILE_NAME);

    if (file.good())
    {
        _loadBasic(file);

        if (file.good())
            _load(file);
    }

    loaded = saved = file.good();

    return saved;
}

bool Settings::Save()
{
    if (!saved && loaded)
    {
        std::ofstream file(CONFIG_FILE_NAME);

        if (file.good())
        {
            _saveBasic(file);
            _save(file);
            file << "\n";
        }

        saved = file.good();
    }

    return saved;
}

void Settings::GenerateDefault()
{
    fields[SETTING_SCREEN_W].unsign = 640;
    fields[SETTING_SCREEN_H].unsign = 480;

    cstrings[SETTING_FONT_ADDRESS] = "C:\\Windows\\Fonts\\arial.ttf";

    saved = false;
    loaded = true;
}

uint64 Settings::GetUnsignedData(SettingsIndex index)
{
    return fields[index].unsign;
}

int64 Settings::GetSignedData(SettingsIndex index)
{
    return fields[index].sing;
}

float64 Settings::GetFloatData(SettingsIndex index)
{
    return fields[index].floa;
}

cstring NULLABLE Settings::GetCStringData(SettingsIndex index)
{
    return cstrings[index];
}

void Settings::_loadBasic(std::ifstream& file)
{
    fields[SETTING_SCREEN_W].unsign = loadUnsigned(file);
    fields[SETTING_SCREEN_H].unsign = loadUnsigned(file);
    cstrings[SETTING_FONT_ADDRESS] = loadCString(file);
}

void Settings::_saveBasic(std::ofstream& file)
{
    file << "RESOLUTION_W " << fields[SETTING_SCREEN_W].unsign << std::endl;
    file << "RESOLUTION_H " << fields[SETTING_SCREEN_H].unsign << std::endl;
    file << "FONT " << cstrings[SETTING_FONT_ADDRESS] << std::endl;
}

uint16 Settings::_getFieldSize()
{
    return SETTING_MAX;
}

uint16 Settings::_getCStringFieldSize()
{
    return SETTING_CSTRING_MAX;
}

uint64 Settings::loadUnsigned(std::ifstream & file)
{
    return _loadField<uint64>(file);
}

int64 Settings::loadSigned(std::ifstream & file)
{
    return _loadField<int64>(file);
}

float64 Settings::loadFloat(std::ifstream & file)
{
    return _loadField<float64>(file);
}

cstring Settings::loadCString(std::ifstream & file)
{
    std::string word;

    //get rid of field name
    file >> word;

    std::getline(file, word);
    if (word.size() > 2 && word.at(0) == ' ')
        word.erase(0, 1);

    char* res = prepareCString(word.length());

    std::copy(word.begin(), word.end(), res);

    return res;
}

template<typename T>
T Settings::_loadField(std::ifstream & file)
{
    std::string word;
    T res;

    file >> word;
    file >> res;
    std::getline(file, word);

    return res;
}

