#ifndef M2D_SHADER_H
#define M2D_SHADER_H
#include "defines.h"

//we need those!
class MSHSimpleColored;
class MSHText;
class MSHTexture;

typedef uint8 ShaderType;
enum ShaderTypes
{
    SHADER_SIMPLE_COLOR,
    SHADER_TEXTURE,
    SHADER_TEXT,
    SHADER_TYPES_MAX
};

typedef uint8 ShaderError;
enum ShaderErrors
{
    SHADER_ERROR_NONE,
    SHADER_ERROR_NON_INITIALIZED,
    SHADER_ERROR_INVALID_ID,
    SHADER_ERROR_VERTEX_SHADER_COMPILATION_FAILED,
    SHADER_ERROR_FRAGMENT_SHADER_COMPILATION_FAILED,
    SHADER_ERROR_PROGRAM_LINK_FAILED,
    SHADER_ERROR_MAX
};

typedef uint8 ShaderCodeId;

//Shader is the class containing information about GPU Programs
class Shader
{
public:
    virtual ~Shader();

    //use this only once!
    //consequences are too high to handle!
    ShaderError InitShader(cstring& errorString);
    virtual int32 AfterInit() = 0;

    //Usage
    void Use();

    //getters
	unsigned GetBuffer(uint8 index) const;
    ShaderType GetType() const;

    //database getters
    ccstring GetName() const;

    //static database
    static ccstring GetName(ShaderType type);

    //be sure about the type of the shader before you cast it!
    MSHSimpleColored* NULLABLE ToSimpleColoredMSH();
    MSHText* NULLABLE ToTextMSH();
    MSHTexture* NULLABLE ToTextureMSH();

    //creator
    static Shader* CreateNewInstance(ShaderType type);

protected:
    Shader(const ShaderType type);

    /// Fields
    const ShaderType type;

    //pointers to attributes in the shader
    //[0] - is always programID
    int64* buffer;

private:
    //helpers
    void _getErrorStringForShader(cstring& errorString, int shaderId) const;
        
    template<class T>
    T* castTo();

    // Database
    ccstring NULLABLE _GetVertexCode() const;
    ccstring NULLABLE _GetFragmentCode() const;
    ShaderCodeId _GetVertexId() const;
    ShaderCodeId _GetFragmentId() const;

    static ccstring NULLABLE _GetCode(ShaderType type);
    static ShaderCodeId _GetVertexId(ShaderType type);
    static ShaderCodeId _GetFragmentId(ShaderType type);
};

#endif //!M2D_SHADER_H
