#ifndef M2D_RENDERTARGET_H
#define M2D_RENDERTARGET_H

#include "Texture.h"

class RenderTarget
{
	unsigned framebufferId;
	Texture* texture;

public:
	RenderTarget(uint32 w, uint32 h, uint32 format = 0x1907 /*RGB*/, uint32 type = 0x1401 /*uint8*/);
	~RenderTarget();

	bool CheckError();

	unsigned GetBuffer() const;
	Texture* GetTexture() const;
};

#endif
