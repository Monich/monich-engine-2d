#include "defines.h"

#include <cstring>
#include <cstdio>

cstring TODELETE prepareCString(uint32 lenght)
{
    cstring res = new char[lenght + 1];
    res[lenght] = '\0';
    return res;
}

cstring TODELETE copyCString(ccstring other)
{
	cstring res = prepareCString(strlen(other));
	strcpy(res, other);
	return res;
}

#ifndef __STDC_LIB_EXT1__

bool fopen_s(FILE **file, ccstring nameFile, ccstring mode)
{
    *file = fopen(nameFile, mode);
    return *file;
}

#endif // !__STDC_LIB_EXT1__